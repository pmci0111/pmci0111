package BibliotecaApp.model.repo;

import BibliotecaApp.control.BibliotecaCtrl;
import BibliotecaApp.model.Carte;
import BibliotecaApp.repository.repoInterfaces.CartiRepoInterface;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

public class CartiRepoTest {
    private BibliotecaCtrl bc;
    private CartiRepoInterface cr;
    @org.junit.Test
    public void adaugaCarteTest() throws Exception {
        Carte c = new Carte();
        c.setTitlu("Carte Test");
        c.setAnAparitie("2019");
        c.adaugaAutor("Vasile");
        c.adaugaCuvantCheie("test");
        try {
            bc.adaugaCarte(c);
        } catch (Exception e) {
            e.printStackTrace();
        }
        Carte rezultat = new Carte();
        rezultat=bc.cautaCarte("Vasile").get(0);
       assertTrue(rezultat.equals(c));

    }



    @org.junit.Test
    public void getCarti() {
    }

    @org.junit.Test
    public void modificaCarte() {
    }

    @org.junit.Test
    public void stergeCarte() {
    }

    @org.junit.Test
    public void cautaCarte() {
    }

    @org.junit.Test
    public void getCartiOrdonateDinAnul() {
    }
}