package BibliotecaApp.control;
import BibliotecaApp.model.Carte;
import BibliotecaApp.model.repo.CartiRepo;
import BibliotecaApp.repository.repoInterfaces.CartiRepoInterface;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.*;

public class BibliotecaCtrlTest {
    CartiRepoInterface cr = new CartiRepo();
    BibliotecaCtrl bc = new BibliotecaCtrl(cr);
    public boolean found=false;
    public boolean found2=false;

    @Test
    public void adaugaCarteValid() {
        Carte c = new Carte();
        c = new Carte();
        List<String> autori = new ArrayList<String>();
        autori.add("Mateiu Caragiale");
        List<String> cuvinteCheie = new ArrayList<String>();
        cuvinteCheie.add("mateiu");
        cuvinteCheie.add("crailor");
        c.setTitlu("abc");
        c.setAutori(autori);
        c.setAnAparitie("2017");
        c.setEditura("Litera");
        c.setCuvinteCheie(cuvinteCheie);
        try {
            bc.adaugaCarte(c);
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            for (Carte o : bc.getCarti()) {
                if (o.equals(c)) {
                    found = true;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        assertTrue(found);
    }
    @Test
    public void adaugaCarteValid2() {
        Carte c2;
        c2 = new Carte();
        List<String> autori2 = new ArrayList<String>();
        autori2.add("Mateiu Vasile");
        List<String> cuvinteCheie2 = new ArrayList<String>();
        cuvinteCheie2.add("mateiu");
        cuvinteCheie2.add("crailor");
        c2.setTitlu("zzz");
        c2.setAutori(autori2);
        c2.setAnAparitie("0");
        c2.setEditura("Litera");
        c2.setCuvinteCheie(cuvinteCheie2);
        try {
            bc.adaugaCarte(c2);
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            for (Carte o : bc.getCarti()) {
                if (o.equals(c2)) {
                    found2 = true;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        assertEquals(false, !found2);
    }
    @Test
    public void adaugaCarteTitluInvalid() {
        Carte c3;
        c3 = new Carte();
        try {
            bc.adaugaCarte(c3);
            fail();

        } catch (Exception e) {
            assertThat(e.getMessage(), is("Titlu invalid!"));
            ;
        }
    }
    @Test
    public void adaugaCarteAnInvalid() {
        Carte c4;
        c4 = new Carte();
        List<String> autori4 = new ArrayList<String>();
        autori4.add("test");
        List<String> cuvinteCheie4 = new ArrayList<String>();
        cuvinteCheie4.add("test");
        cuvinteCheie4.add("test");
        c4.setTitlu("zzz");
        c4.setAutori(autori4);
        c4.setAnAparitie("-1");
        c4.setEditura("Litera");
        c4.setCuvinteCheie(cuvinteCheie4);
        try {
            bc.adaugaCarte(c4);
            fail();

        } catch (Exception e) {
            assertThat(e.getMessage(), is("An aparitie invalid!"));;
        }
        try {
            bc.getCarti();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    @Test
    public void modificaCarte() {
    }

    @Test
    public void deleteCarte() {
    }

    @Test
    public void cautaCarte() {
    }

    @Test
    public void getCarti() {
    }

    @Test
    public void getCartiOrdonateDinAnul() {
    }
}