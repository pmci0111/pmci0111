package BibliotecaApp.repository.repoMock;

import BibliotecaApp.model.Carte;
import BibliotecaApp.repository.repoMock.CartiRepoMock;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

public class CartiRepoMockTest {

    @Test
    public void cautaCarte1() {
        /*TC01*/
        CartiRepoMock mock = new CartiRepoMock();

        ArrayList<Carte> carti = new ArrayList<Carte>();
        ArrayList<Carte> carti2 = new ArrayList<Carte>();
        assertEquals(mock.cautaCarte("caragiale"), carti2);
        /*TC02*/
    }
    @Test
    public void cautaCarte2() {
        CartiRepoMock mock = new CartiRepoMock();
        ArrayList<Carte> carti2 = new ArrayList<Carte>();
        Carte c2 = new Carte();
        List<String> autori2 = new ArrayList<String>();
        autori2.add("Mateiu Vasile");
        List<String> cuvinteCheie2 = new ArrayList<String>();
        cuvinteCheie2.add("mateiu");
        cuvinteCheie2.add("crailor");
        c2.setTitlu("zzz");
        c2.setAutori(autori2);
        c2.setAnAparitie("0");
        c2.setEditura("Litera");
        c2.setCuvinteCheie(cuvinteCheie2);
        try {
            mock.adaugaCarte(c2);
        } catch (Exception e) {
            e.printStackTrace();
        }
        carti2.add(c2);
        assertEquals(mock.cautaCarte("mateiu"), carti2);
    }

    /*TC03*/
    @Test
    public void cautaCarte3() {
        CartiRepoMock mock2 = new CartiRepoMock();
        ArrayList<Carte> carti2 = new ArrayList<Carte>();
        Carte c3 = new Carte();
        List<String> autori3 = new ArrayList<String>();
        autori3.add("Mateiu Vasile");
        List<String> cuvinteCheie3 = new ArrayList<String>();
        cuvinteCheie3.add("vasile");
        cuvinteCheie3.add("crailor");
        c3.setTitlu("zzz");
        c3.setAutori(autori3);
        c3.setAnAparitie("0");
        c3.setEditura("Litera");
        c3.setCuvinteCheie(cuvinteCheie3);

        Carte c2 = new Carte();
        List<String> autori2 = new ArrayList<String>();
        autori2.add("Mateiu Vasile");
        List<String> cuvinteCheie2 = new ArrayList<String>();
        cuvinteCheie2.add("mateiu");
        cuvinteCheie2.add("crailor");
        c2.setTitlu("zzz");
        c2.setAutori(autori2);
        c2.setAnAparitie("0");
        c2.setEditura("Litera");
        c2.setCuvinteCheie(cuvinteCheie2);

        carti2.add(c2);
        try {
            mock2.adaugaCarte(c3);
            mock2.adaugaCarte(c2);
        } catch (Exception e) {
            e.printStackTrace();
        }
        assertEquals(mock2.cautaCarte("mateiu"), carti2);
    }

}